<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles');
function my_theme_enqueue_styles(){
	wp_enqueue_style('parent', get_template_directory_uri() . '/style.css');
}


function about_us(){
	return "<p id='about_text_shortcode' >Our company offers sports clothing and accessories for everyone!
	Our Aim is to help you to Stay Fit and Stay Healthy in your daily Routine</p>";
}


function opening_hours($atts){
	extract(shortcode_atts(array("op1" => '',"op2" => ''), $atts));
	return " <p><strong> Mon-Fri : ".$op1." <br> </p>
			 <p> <strong> Saturdays : ".$op2." </p>";
}
function brands_table(){
	return '<table id="short_table"><tr><th>Brand</th><th>About the Brand</th>
			<tr>
			<td><a href="https://www.nike.com/" target = "_blank"><img src = "https://i.pinimg.com/originals/d6/00/0d/d6000de9de3547cdb98ca7f7ef85ddd8.jpg" alt = "nike logo" height  = "150" width = "150"> </a></td>
			<td> <p>Nike is an American multinational corporation that is engaged in the design, development, manufacturing, 
			and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services. 
			The company is headquartered near Beaverton, Oregon, in the Portland metropolitan area. 
			It is the world  largest supplier of athletic shoes and a major manufacturer of sports equipment </p> </td>
			
			</tr>
			
			<tr>
			<td><a href="https://www.adidas.com/" target = "_blank"><img src = "https://i.pinimg.com/564x/62/c3/07/62c3078e2a6296802b826bbf1f4b715e.jpg" alt = "adidas logo" height  = "100" width = "100"> </a></td>
			<td> <p>Adidas since 1949 is a multinational corporation, founded and headquartered in Herzogenaurach, 
			Germany, that designs and manufactures shoes, clothing and accessories. 
			It is the largest sportswear manufacturer in Europe, and the second largest in the world, after Nike. 
			It is the holding company for the Adidas Group, which consists of the Reebok sportswear company, 8.33% of the German football club Bayern Munich, and Runtastic, 
			an Austrian fitness technology company</p> </td>
			
			</tr>
			
			<tr>
			<td><a href="https://www.fila.com/" target = "_blank"><img src = "https://i.pinimg.com/564x/78/0a/f8/780af8497a48a6298de9941ba4c860a0.jpg" alt = "fila logo" height  = "100" width = "100"> </a></td>
			<td><p>Fila Holdings Corp. is a South Korean corporation that designs and manufactures shoes and apparel, 
			taking its current name after the acquisition of the Italian company Fila in 2007 and therefore the rights to the brand name. 
			It is the largest South Korean sportswear manufacturer. Fila was originally founded by Ettore and Giansevero Fila in 1911 in Biella, Piedmont, 			
			Italy In 2003, it was sold to United States-based Sports Brand International. In 2007, the company was sold to Fila Korea, its South Korean division.
			In September 2010, it launched its initial public offering on the Korea Exchange.</p> </td>
			
			</tr></table>';
			
}



function social_media(){
	return '<p> <strong> Follow Us On: <a id="facebook_link" href="https://www.facebook.com/" target= "_blank">Our Facebook Page</a> 
	or  <a id="facebook_link" href="https://www.instagram.com/" target= "_blank">Our Instagram Page</a>';
}

function subscribe_button(){
	return '<h2> Subscribe to our Newslatter </h2> <button class = "subscribe_button" type = "button" onclick= "alert(You Have Been Subscirbed To our Newslatter!)">Subscribe!</button>';
}

function shipping_info(){
	return '<h1> Shipping and Returns <img src = "https://www.maktabaislamia.com/media/wysiwyg/title-shipping.jpg" alt = "shipping and returns" height = "200" width = "200"> <br></h1> 
	 <ul>
	 <li><p>Returns shall be guaranteed for up to <strong> TWO  weeks  </strong>  on any purchased item  </p> </li>
	 <li>Any returns, unless faulty, have to be returned in a <strong> re-saleable condition </strong>. This means that all original packaging and labels need to be flawlessly returned and that product is unused. </p> </li>
	 <li> <strong> Personalised products </strong>, unless faulty, are not eligible for refund/exchange.  </p> </li>
	 </ul>
	';
}
function delivery_info($atts){
	extract(shortcode_atts(array("malta" => '',"gozo" => ''), $atts));
	return " <h1> Delivery Information <img src = 'https://vajro.com/wp-content/uploads/2018/07/9-Fundamentals-About-Shipping-for-E-commerce-Stores.jpg' 
	alt = 'delivery' height= '200' 
	width = '200'> <br> </h1>
	<h3> Deliveries are <strong> Free of charge </strong> and may take <strong> Five (5) </strong> to <strong> Six (6) 
	</strong> Working days <br> </h3>
	<table id = 'delivery_table'> 
	<tr> <th> Malta </th> <th> Gozo </th> </tr>
	<tr> <td> ".$malta." </td> 
	 <td> ".$gozo." </td> </tr>
	
	</table>
	";
}



add_shortcode('About_Us', 'about_us');
add_shortcode('Opening_Hours', 'opening_hours');
add_shortcode('Social_Media', 'social_media');
add_shortcode('Brands_Table', 'brands_table');
add_shortcode('Subscribe_Button', 'subscribe_button');
add_shortcode('Shipping_Info' , 'shipping_info');
add_shortcode('Delivery_Info' , 'delivery_info');
?>
